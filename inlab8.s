	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 12
	.intel_syntax noprefix
	.globl	__Z8mathFunciRicf
	.align	4, 0x90
__Z8mathFunciRicf:                      	## @_Z8mathFunciRicf
## BB#0:
	push	ebp
	mov	ebp, esp
	sub	esp, 16
	movss	xmm0, dword ptr [ebp + 20] 	## xmm0 = mem[0],zero,zero,zero
	mov	al, byte ptr [ebp + 16]
	mov	ecx, dword ptr [ebp + 12]
	mov	edx, dword ptr [ebp + 8]
	mov	dword ptr [ebp - 4], edx
	mov	dword ptr [ebp - 8], ecx
	mov	byte ptr [ebp - 9], al
	movss	dword ptr [ebp - 16], xmm0
	mov	ecx, dword ptr [ebp - 4]
	mov	edx, dword ptr [ebp - 8]
	add	ecx, dword ptr [edx]
	movsx	edx, byte ptr [ebp - 9]
	add	ecx, edx
	cvtsi2ss	xmm0, ecx
	addss	xmm0, dword ptr [ebp - 16]
	cvttss2si	eax, xmm0
	add	esp, 16
	pop	ebp
	ret

	.section	__TEXT,__literal4,4byte_literals
	.align	2
LCPI1_0:
	.long	1085585070              ## float 5.64729977
	.section	__TEXT,__text,regular,pure_instructions
	.globl	_main
	.align	4, 0x90
_main:                                  			## @main
## BB#0:
	push	ebp
	mov	ebp, esp
	sub	esp, 40
	call	L1$pb
L1$pb:
	pop	eax
	lea	ecx, [ebp - 12]
	movss	xmm0, dword ptr [eax + LCPI1_0-L1$pb] 		## xmm0 = mem[0],zero,zero,zero
	mov	dword ptr [ebp - 4], 0
	mov	dword ptr [ebp - 8], 5
	mov	dword ptr [ebp - 12], 10
	mov	byte ptr [ebp - 13], 97
	movss	dword ptr [ebp - 20], xmm0
	mov	eax, dword ptr [ebp - 8]
	mov	dl, byte ptr [ebp - 13]
	movss	xmm0, dword ptr [ebp - 20] 			## xmm0 = mem[0],zero,zero,zero
	mov	dword ptr [esp], eax
	mov	dword ptr [esp + 4], ecx
	movsx	eax, dl
	mov	dword ptr [esp + 8], eax
	movss	dword ptr [esp + 12], xmm0
	call	__Z8mathFunciRicf
	add	esp, 40
	pop	ebp
	ret


.subsections_via_symbols
