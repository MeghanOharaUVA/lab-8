#ifndef OBJECT8_H
#define OBJECT8_H

#include <string>

using namespace std;

class myobject {
 public:
  myobject();
  int getI(); 
  float f;
  char *cr; 

 private: 
  string value;
  int i;
  int *ir; 
  char c;
  friend class myobject; 
  
};

#endif
