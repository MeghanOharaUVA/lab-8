class object8 {
public:
  int i;
  float *fr;
  char c;

private:
  int *ir;
  float f;
  char *cr;
};

int main() {
  object8 obj8;

  int d = 1;
  float e = 0.0;
  char f = 'a';

  obj8.i = d;
  obj8.f = e;
  obj8.c = f;

  obj8.ir = &d;
  obj8.fr = &e;
  obj8.cr = &f;
}
