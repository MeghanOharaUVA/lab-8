// Meghan O'Hara mmo2bf 10/30/16 mathfun.cpp

#include <iostream>
#include <cstdlib>

using namespace std;

extern "C" int product (int, int);
extern "C" int power (int, int); 


int main() {
  int x, y;
  cout << "Please enter a value for x: ";
  cin >> x;

  cout << "Please enter a value for y: ";
  cin >> y;

  int prod = product(x, y);
  cout << "The product of x & y is: " << prod << endl;

  int pow = power(x, y);
  cout << "x to the power of y is: " << pow << endl; 

} 
