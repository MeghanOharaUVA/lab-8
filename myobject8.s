	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 12
	.intel_syntax noprefix
	.globl	__ZN9myobject8C2Ev
	.align	4, 0x90
__ZN9myobject8C2Ev:                     ## @_ZN9myobject8C2Ev
## BB#0:
	push	ebp
	mov	ebp, esp
	push	ebx
	push	esi
	sub	esp, 16
	mov	eax, dword ptr [ebp + 8]
	lea	ecx, [ebp - 17]
	lea	edx, [ebp - 16]
	xorps	xmm0, xmm0
	mov	dword ptr [ebp - 12], eax
	mov	eax, dword ptr [ebp - 12]
	mov	dword ptr [ebp - 16], 18
	mov	byte ptr [ebp - 17], 99
	movss	dword ptr [ebp - 24], xmm0
	mov	esi, dword ptr [ebp - 16]
	mov	dword ptr [eax + 8], esi
	mov	bl, byte ptr [ebp - 17]
	mov	byte ptr [eax + 16], bl
	movss	xmm0, dword ptr [ebp - 24] ## xmm0 = mem[0],zero,zero,zero
	movss	dword ptr [eax], xmm0
	mov	dword ptr [eax + 12], edx
	mov	dword ptr [eax + 4], ecx
	add	esp, 16
	pop	esi
	pop	ebx
	pop	ebp
	ret

	.globl	__ZN9myobject8C1Ev
	.align	4, 0x90
__ZN9myobject8C1Ev:                     ## @_ZN9myobject8C1Ev
## BB#0:
	push	ebp
	mov	ebp, esp
	sub	esp, 8
	mov	eax, dword ptr [ebp + 8]
	mov	dword ptr [ebp - 4], eax
	mov	eax, dword ptr [ebp - 4]
	mov	dword ptr [esp], eax
	call	__ZN9myobject8C2Ev
	add	esp, 8
	pop	ebp
	ret

	.globl	__ZN9myobject84getIEv
	.align	4, 0x90
__ZN9myobject84getIEv:                  ## @_ZN9myobject84getIEv
## BB#0:
	push	ebp
	mov	ebp, esp
	sub	esp, 8
	mov	eax, dword ptr [ebp + 8]
	mov	dword ptr [ebp - 4], eax
	movss	xmm0, dword ptr [eax]   ## xmm0 = mem[0],zero,zero,zero
	movss	dword ptr [ebp - 8], xmm0
	fld	dword ptr [ebp - 8]
	add	esp, 8
	pop	ebp
	ret

	.globl	_main
	.align	4, 0x90
_main:                                  ## @main
## BB#0:
	push	ebp
	mov	ebp, esp
	sub	esp, 40
	lea	eax, [ebp - 24]
	mov	dword ptr [ebp - 4], 0
	mov	dword ptr [esp], eax
	call	__ZN9myobject8C1Ev
	lea	eax, [ebp - 24]
	mov	dword ptr [esp], eax
	call	__ZN9myobject84getIEv
	fstp	dword ptr [ebp - 28]
	movss	xmm0, dword ptr [ebp - 28]         ## xmm0 = mem[0],zero,zero,zero
	cvttss2si	eax, dword ptr [ebp - 24]
	movss	dword ptr [ebp - 32], xmm0         ## 4-byte Spill
	add	esp, 40
	pop	ebp
	ret


.subsections_via_symbols
