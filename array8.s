	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 12
	.intel_syntax noprefix
	.globl	__Z7arrFuncPii
	.align	4, 0x90
__Z7arrFuncPii:                         ## @_Z7arrFuncPii
## BB#0:
	push	ebp
	mov	ebp, esp
	sub	esp, 8
	mov	eax, dword ptr [ebp + 12]
	mov	ecx, dword ptr [ebp + 8]
	mov	dword ptr [ebp - 4], ecx
	mov	dword ptr [ebp - 8], eax
	mov	eax, dword ptr [ebp - 4]
	mov	eax, dword ptr [eax]
	add	esp, 8
	pop	ebp
	ret

	.globl	_main
	.align	4, 0x90
_main:                                  ## @main
## BB#0:
	push	ebp
	mov	ebp, esp
	push	ebx
	push	edi
	push	esi
	sub	esp, 60
	call	L1$pb
L1$pb:
	pop	eax
	mov	ecx, 5
	lea	edx, [ebp - 36]
	lea	esi, [eax + l__ZZ4mainE3arr-L1$pb]
	mov	edi, 20
	mov	ebx, dword ptr [eax + L___stack_chk_guard$non_lazy_ptr-L1$pb]
	mov	ebx, dword ptr [ebx]
	mov	dword ptr [ebp - 16], ebx
	mov	dword ptr [ebp - 40], 0
	mov	ebx, edx
	mov	dword ptr [esp], ebx
	mov	dword ptr [esp + 4], esi
	mov	dword ptr [esp + 8], 20
	mov	dword ptr [ebp - 44], eax ## 4-byte Spill
	mov	dword ptr [ebp - 48], ecx ## 4-byte Spill
	mov	dword ptr [ebp - 52], edx ## 4-byte Spill
	mov	dword ptr [ebp - 56], edi ## 4-byte Spill
	call	_memcpy
	mov	eax, dword ptr [ebp - 52] ## 4-byte Reload
	mov	dword ptr [esp], eax
	mov	dword ptr [esp + 4], 5
	call	__Z7arrFuncPii
	mov	ecx, dword ptr [ebp - 44] ## 4-byte Reload
	mov	edx, dword ptr [ecx + L___stack_chk_guard$non_lazy_ptr-L1$pb]
	mov	edx, dword ptr [edx]
	cmp	edx, dword ptr [ebp - 16]
	mov	dword ptr [ebp - 60], eax ## 4-byte Spill
	jne	LBB1_2
## BB#1:
	mov	eax, dword ptr [ebp - 60] ## 4-byte Reload
	add	esp, 60
	pop	esi
	pop	edi
	pop	ebx
	pop	ebp
	ret
LBB1_2:
	call	___stack_chk_fail

	.section	__TEXT,__const
	.align	2                       ## @_ZZ4mainE3arr
l__ZZ4mainE3arr:
	.long	1000                    ## 0x3e8
	.long	2                       ## 0x2
	.long	3                       ## 0x3
	.long	17                      ## 0x11
	.long	50                      ## 0x32


	.section	__IMPORT,__pointers,non_lazy_symbol_pointers
L___stack_chk_guard$non_lazy_ptr:
	.indirect_symbol	___stack_chk_guard
	.long	0

.subsections_via_symbols
