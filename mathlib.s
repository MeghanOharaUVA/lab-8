;  Meghan O'Hara mmo2bf 10/30/16 mathlib.s   


	global product
	global power 

	section .text
	
;
; mathlib.s
; x  - first int provided - base in power(x, y)
; y  - second int provided - exp in power(x, y)
; Return value - the product in product(x, y) - the power in power(x, y)
;



; first subroutine - product(x, y)
product:
	; Standard prologue
	; Taken from vecsum.s 
	push  ebp		; Save the old base pointer
	mov   ebp, esp		; Set new value of the base pointer
	push  esi		; Save registers

	xor   eax, eax		; Place zero in EAX. (ans)

	mov   esi, [ebp+8]	; x 
	mov   ecx, [ebp+12]	; y 
	
product_loop:
	cmp ecx, 0		; if (y<= 0)
	jle product_done

	add eax, esi		; ans += x
	dec ecx 		; y--
	jmp product_loop	; end loop

product_done:
	; Standard epilogue   
	; Taken from vecsum.s
	pop   esi		; Restore registers that we used.
				; Note - no local variables to dealocate.
	pop   ebp		; Restore the caller's base pointer.
	ret			; Return to the caller.


; second subroutine - power(x, y)
power:
	; Standard prologue 
	; Taken from vecsum.s
	push  ebp		; Save the old base pointer
	mov   ebp, esp		; Set new value of the base pointer
	push  esi		; Save registers
	
	mov   esi, [ebp+8]	; x
	mov   ecx, [ebp+12]	; y


power_recursive:
	cmp   ecx, 1		; if (y <= 1)
	jle   power_done
	
	dec   ecx		; y--

	push  ecx		; push params in reverse order 
	push  esi

	call  power_recursive	; recursion

        pop   esi    		; remove parameters from stack
	pop   ecx


	push  esi		; push params in reverse order 
	push  eax
	
	call  product		; product(retval, x)
	
        pop   eax      		; remove parameters from stack
	pop   esi 
	
	ret 

power_done:
	; Standard epilogue   
	; Taken from vecsum.s
	mov   eax, esi     	; store x as return value
	pop   esi		; Restore registers that we used.
				; Note - no local variables to dealocate.
	pop   ebp		; Restore the caller's base pointer.
	ret			; Return to the caller.
