#ifndef OBJECT_H
#define OBJECT_H

#include <string>

using namespace std;

class Object {
public:
    Object();						//Default Constructor
    Object(const string & val);	//Constructor

private:
    string value;
    Object *left, *right;			// for trees
    friend class inlab;			//gives TreeCalc access to private data
};

#endif
